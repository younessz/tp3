package fr.uavignon.ceri.tp3.data;

public class WeatherResult {

    static void transferInfo(WeatherResponse weatherInfo, City cityInfo){


        cityInfo.setDescription(weatherInfo.weather.get(0).description);
        cityInfo.setIcon(weatherInfo.weather.get(0).icon);
        cityInfo.setHumidity(weatherInfo.main.humidity);
        cityInfo.setTemperature(weatherInfo.main.temp);
        cityInfo.setCloudiness(weatherInfo.clouds.all);
        cityInfo.setWindSpeedMPerS(weatherInfo.wind.speed);
        cityInfo.setLastUpdate(weatherInfo.dt);

    }
}
