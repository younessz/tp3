package fr.uavignon.ceri.tp3.data;

import java.util.List;

public class WeatherResponse {

    public final List <Weather> weather=null;
    public static class Weather {
        String description;
        String icon;

    }

    public final Main main=null;
    public static class Main {
        int humidity;
        float temp;
    }

    public final Wind wind=null;
    public static class Wind {
        float speed ;
    }

    public final Clouds clouds=null;
    public static class Clouds {
        int all ;
    }

    public final Integer dt=null;

}